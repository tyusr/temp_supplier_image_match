# -*- coding: utf-8 -*-
# @Author  : tuoyu@clubfactory.com
import json
import pandas as pd
from tqdm import trange, tqdm
from functools import lru_cache
from collections import defaultdict
from supplier_image_match.match import get_phash_from_urls, is_matched, phash_to_b64str
from supplier_image_match.orm import ClusterNodes
from supplier_image_match.logger import Logger
from club_model.helper.database import session_scope
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from time import time

batch_size = 1000

engine_etl_image_match = create_engine(
    r'mysql+mysqlconnector://'
    r'readonly:ardeoanly2-epiCLOUDS2018@10.66.228.228:3306',
    pool_pre_ping=True
)
sess_maker = sessionmaker(bind=engine_etl_image_match)

logger = Logger('profile', './').get_logger()


def get_mongo_client():
    import traceback
    from pymongo import MongoClient
    try:
        mongo_client = MongoClient('10.104.114.244', 27017)
        mongo_client.admin.authenticate('mongoreader', 'adckjlkje-epiCLOUDS2018')
        return mongo_client
    except Exception as e:
        print(traceback.format_exc())
        return None


@lru_cache(16)
def get_collection(category):
    return get_mongo_client()['alibaba'][category]


def get_images_from_doc(doc):
    image_set = set()
    first_image = None
    item_id = doc['itemId']

    if 'itemPicUrls' in doc:
        image_field = doc['itemPicUrls']
        if isinstance(image_field, list):
            image_set.update(image_field)
            if len(image_field) > 0:
                first_image = image_field[0]
        elif isinstance(image_field, str):
            image_set.add(image_field)
            first_image = image_field

    if 'skuItems' in doc:
        sku_field = doc['skuItems']
        if sku_field:
            for sku in sku_field:
                if 'info' not in sku or 'imageUrl' not in sku['info']:
                    continue
                image_set.add(sku['info']['imageUrl'])
    if first_image in image_set:
        image_set.remove(first_image)
    return item_id, [first_image] + list(image_set)


def process(df, min_id, max_id):
    # link_id  node_id  pid  image_url
    n_iter = len(df) // batch_size + 1

    if min_id is None:
        min_id = 0
    if max_id is None:
        max_id = n_iter

    for n in trange(n_iter):
        if not (min_id <= n < max_id):
            continue
        tt = time()

        t = time()
        beg = n * batch_size
        end = beg + batch_size
        _batch = df.iloc[beg:end]
        node_list = _batch.node_id.tolist()
        logger.info(f'[1] {time() - t:.3f} get batch')

        t = time()
        with session_scope(session=sess_maker()) as sess:
            q = sess.query(ClusterNodes.node_id, ClusterNodes.category_name).filter(ClusterNodes.node_id.in_(node_list))
            df_node = pd.read_sql(q.statement, sess.bind)
        logger.info(f'[2] {time() - t:.3f} query cluster nodes')

        batch = pd.merge(df_node, _batch, how='left', on='node_id')
        batch['item_id'] = [node_id.split('-')[-1] for node_id in batch.node_id]

        dst_url_dict = defaultdict(list)
        for category, sub in tqdm(batch.groupby('category_name')):
            collection = get_collection(category)

            t = time()
            query = collection.find({'itemId': {'$in': sub.item_id.tolist()}},
                                    {'itemId': 1,
                                     'itemPicUrls': 1,
                                     'skuItems': 1})
            for doc in query:
                item_id, urls = get_images_from_doc(doc)
                dst_url_dict[item_id] = urls
            logger.info(f'[3-1] {time() - t:.3f} query mongo {len(sub)}')

            image_urls = [str(url) for url in sub.image_url]
            src_url_list = [url.replace("https://s3.amazonaws.com/fromfactory.club.image/",
                                        "http://d1vs5fqeka2glf.cloudfront.net/")
                            if 'fromfactory.club' in url else url
                            for url in image_urls]
            t = time()
            src_phash_list = get_phash_from_urls(src_url_list)
            dst_first_url_list = [dst_url_dict[r['item_id']][0] if len(dst_url_dict[r['item_id']]) > 0 else None
                                  for _, r in sub.iterrows()]
            dst_first_phash_list = get_phash_from_urls(dst_first_url_list)
            logger.info(f'[3-2] {time() - t:.3f} download {len(src_url_list) + len(dst_first_url_list)} images')

            for i, (idx, row) in enumerate(sub.iterrows()):
                src_url = src_url_list[i]
                src_phash = src_phash_list[i]

                dst_urls = dst_url_dict[row['item_id']]
                dst_first_phash = dst_first_phash_list[i]

                t = time()
                flag = is_matched(src_phash, [dst_first_phash])
                if flag:
                    record = dict(
                        link_id=row['link_id'],
                        pid=row['pid'],
                        item_id=row['item_id'],
                        src_url=src_url,
                        src_phash=phash_to_b64str(src_phash),
                        dst_urls=dst_urls,
                        dst_phashs=[phash_to_b64str(p) for p in [dst_first_phash]],
                        is_matched=flag
                    )
                    logger.info(f'[3-3-a] {time() - t:.3f} first image matched')
                else:
                    dst_phashs = get_phash_from_urls(dst_urls)
                    record = dict(
                        link_id=row['link_id'],
                        pid=row['pid'],
                        item_id=row['item_id'],
                        src_url=src_url,
                        src_phash=phash_to_b64str(src_phash),
                        dst_urls=dst_urls,
                        dst_phashs=[phash_to_b64str(p) for p in dst_phashs],
                        is_matched=is_matched(src_phash, dst_phashs)
                    )
                    logger.info(f'[3-3-b] {time() - t:.3f} down&compute {len(dst_urls)} images')
                with open(f'./data/match_results/{record["link_id"]}.json', 'w') as f:
                    json.dump(record, f)
        logger.info(f'[end] {time() - tt:.3f} total')

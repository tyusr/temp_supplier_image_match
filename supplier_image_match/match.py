# !/usr/bin/env python

import cv2
import base64
from club_model.helper.image import download_image
from concurrent.futures import ThreadPoolExecutor, as_completed

__phash = cv2.img_hash.PHash_create()


#
# def download_images(urls):
#     result = {}
#     with ThreadPoolExecutor(max_workers=4) as executor:
#         future_to_phash = {executor.submit(download_image, url): url for url in urls}
#         for future in as_completed(future_to_phash):
#             url = future_to_phash[future]
#             try:
#                 phash_value = future.result()
#                 result[url] = phash_value
#             except Exception as exc:
#                 result[url] = None
#         results = [result[url] for url in urls]
#     return results

def download_images(urls):
    with ThreadPoolExecutor(max_workers=4) as executor:
        results = executor.map(download_image, urls)
    return list(results)


def phash_to_b64str(p):
    if p is None:
        return None
    return base64.b64encode(bytes(p)).decode()


def get_phash_from_urls(urls):
    imgs = download_images([url for url in urls])
    return [__phash.compute(img) if img is not None else None for img in imgs]


def is_matched(query, gallery: list):
    if query is None:
        return False
    _gallery = [g for g in gallery if g is not None]
    for g in _gallery:
        if __phash.compare(query, g) <= 2:
            return True
    return False

# -*- coding: utf-8 -*-
# @Author  : tuoyu@clubfactory.com
import os
import sys

sys.path.append(os.getcwd())


def main():
    from argparse import ArgumentParser

    parser = ArgumentParser()
    group_model = parser.add_mutually_exclusive_group()
    group_model.add_argument('--csv', type=str, default='')

    parser.add_argument('--min_id', type=int, default=None)
    parser.add_argument('--max_id', type=int, default=None)

    args = parser.parse_args()
    print(args)

    if args.csv:
        import pandas as pd
        from supplier_image_match.process import process
        df = pd.read_csv(args.csv)
        process(df, args.min_id, args.max_id)

    return


if __name__ == '__main__':
    main()

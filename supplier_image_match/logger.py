# -*- coding: utf-8 -*-
# @Author  : tuoyu@clubfactory.com
import socket
import logging
from logging import handlers
from os import makedirs
from pathlib import Path


class Logger:

    def __init__(self, name='sku_color_match', save_dir=''):
        # 创建一个logger
        self.logger = logging.getLogger(name)
        self.logger.setLevel(logging.DEBUG)

        log_path = Path(f'{save_dir}/{name}.log')
        if not log_path.exists():
            makedirs(log_path.parent, exist_ok=True)

        # 创建一个handler，用于写入日志文件
        fh = handlers.TimedRotatingFileHandler(str(log_path), when='MIDNIGHT', backupCount=10)
        fh.setLevel(logging.DEBUG)

        # 再创建一个handler，用于输出到控制台
        ch = logging.StreamHandler()
        ch.setLevel(logging.ERROR)

        # 定义handler的输出格式
        formatter = logging.Formatter(
            '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)

        # 给logger添加handler
        self.logger.addHandler(fh)
        self.logger.addHandler(ch)

    def get_logger(self):
        return self.logger


def get_host_ip():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(('8.8.8.8', 80))
        ip = s.getsockname()[0]
    finally:
        s.close()

    return ip

# -*- coding: utf-8 -*-
# @Author  : tuoyu@clubfactory.com
from sqlalchemy import (Column,
                        Integer,
                        Float,
                        BigInteger,
                        SmallInteger,
                        String,
                        Text,
                        ForeignKey,
                        Numeric,
                        Boolean,
                        DateTime,
                        TIMESTAMP,
                        VARCHAR,
                        PrimaryKeyConstraint,
                        UniqueConstraint,
                        Index,
                        Time,
                        JSON)
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.dialects.mysql import insert
from club_model.helper.database import Base
from datetime import datetime


class ClusterNodes(Base):
    """
    cluster_node主表;
    """
    __tablename__ = 'cluster_nodes'
    __table_args__ = (
        UniqueConstraint('node_id', name='node_id'),
        UniqueConstraint('seq_num', name='seq_num'),
        {'schema': 'image_match'}
    )
    id = Column(Integer, primary_key=True, autoincrement=True)
    cluster_id = Column(Integer, index=True)
    item_id = Column(VARCHAR(128), index=True)
    node_id = Column(VARCHAR(128), index=True)
    platform = Column(VARCHAR(64), index=True)
    url = Column(Text)
    category_id = Column(VARCHAR(32), index=True)
    category_name = Column(VARCHAR(64), index=True)
    seq_num = Column(Integer, index=True)
    item_status = Column(SmallInteger, default=0)
    sold_number = Column(Integer, default=0)
    price = Column(Float)
    price_range = Column(VARCHAR(64))
    is_root = Column(SmallInteger, default=0)
    insert_time = Column(TIMESTAMP, default=datetime.now)
    update_time = Column(TIMESTAMP, default=datetime.now)

    active = Column(SmallInteger, default=1, index=True)
